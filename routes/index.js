var express = require('express');
var router = express.Router();

/* GET Home Page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET Zone Form. */
router.get('/createzone', function(req, res, next) {
  res.render('createzone', null);
});

/* GET Comment Form */
router.get('/createcomment', function(req, res, next) {
  res.render('createcomment', null);
});

module.exports = router;
