import React, { Component } from 'react'
import Comment from './Comment'
import styles from './Styles'

class Comments extends Component{

    constructor(){
        super()

        this.state = {
            list: [
                {body: 'comment 1', username:'dtrump', timestamp:'10:30'},
                {body: 'comment 2', username:'lpeter', timestamp:'10:40'},
                {body: 'comment 3', username:'jdoe', timestamp:'11:00'},
                {body: 'comment 4', username:'gwilson', timestamp:'11:30'}
            ]
        }
    }

    render(){
         const commentStyle = styles.comment;

        const CommentList = this.state.list.map((comment, i) => {
            return (
                <li><Comment currentComment={comment} /></li>
            )
        })

        return(
            <div style={commentStyle.commentBox}>
                <h4>Zone 1: Comments</h4>
                <ul >
                    {CommentList}
                </ul>
            </div>
        )
    }
}

export default Comments