export default {
     universal:{

     },
     comment:{
        commentBox: {
            padding:5, 
            background:'#f9f9f9', 
            border:'1px solid #ddd'
        }
     },
     zone: {
        container: {
            padding:5, 
            marginBottom:5, 
            background:'#f9f9f9', 
            border:'1px solid #ddd'
        },
        zoneTitle:{
            margin:0
        },
        zoneLink:{
            textDecoration:'none'
        }
     }
}