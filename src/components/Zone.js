import React, {Component} from 'react'
import styles from './Styles'

class Zone extends Component{

    

    render(){

        const zoneStyle = styles.zone;

        return(
            <div style={zoneStyle.container}>
                <h3 style={zoneStyle.zoneTitle}>
                    <a style={zoneStyle.zoneLink} href="#">{this.props.currentZone.name}</a>
                </h3>
                <span>{this.props.currentZone.zipCode}</span><br />
                <span>{this.props.currentZone.numComment} comments</span>
            </div>
        )
    }
}

export default Zone