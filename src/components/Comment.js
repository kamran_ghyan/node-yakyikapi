import React, {Component} from 'react'


class Comment extends Component{

    render(){
        return(
            <div>
                <span>{this.props.currentComment.body}</span><br />
                <span>username : {this.props.currentComment.username}</span><br />
                <span>{this.props.currentComment.timestamp}</span>
            </div>
        )
    }
}

export default Comment